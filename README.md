# Gits CRUD & Auth

## How to Try it
Menggunakan Docker Compose
```
docker-compose -f docker-compose.kumpul.yml up -d
```
*Pastikan port 1323 dan 5432 tersedia  
**Pulling API service image from docker hub.
Postman Docs: https://documenter.getpostman.com/view/17343050/UV5RkKZx  

## Local Compose
**ER Diagram**
![ER Diagram](/er-diagram.png)

Untuk menjalankan tanpa pulling API service image, diharapkan menyalakan database terlebih dahulu dengan menggunakan  
`docker-compose -f docker-compose.yml up -d`  
setelah itu masuk ke dalam folder `src` dan bisa dijalankan dengan menggunakan.
`go run main.go`  
Di dalam file tersebut terdapat beberapa inisialisasi, salah satunya adalah inisialisasi Database, fungsi tersebut memiliki parameter berikut

```
db.Init(tableDelete, dataInitialization bool)
tableDelete: digunakan untuk menghapus table yang sudah ada pada database
dataInitialization: digunakan untuk menambahkan dummy data
```

In summary:  
```
docker-compose -f docker-compose.yml
cd src
go run main.go
```
*Pastikan port 1323 dan 5432 tersedia  
