package entity

type Post struct {
	PostID     uint   `gorm:"primary_key;auto_increment" json:"id"`
	Konten     string `gorm:"not null" json:"konten"`
	PathGambar string `json:"path_gambar"`

	UserID     uint     `json:"user_id"`
	User       User     `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
	KategoriID uint     `json:"kategori_id"`
	Kategori   Kategori `gorm:"foreignKey:KategoriID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
}

func (Post) TableName() string {
	return "post"
}
