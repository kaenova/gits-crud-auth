package entity

type Komentar struct {
	KomentarID uint   `gorm:"primary_key;auto_increment" json:"id"`
	Konten     string `gorm:"not null" json:"konten"`

	UserID uint `json:"user_id"`
	User   User `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
	PostID uint `json:"post_id"`
	Post   Post `gorm:"foreignKey:PostID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
}

func (Komentar) TableName() string {
	return "komentar"
}
