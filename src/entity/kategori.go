package entity

type Kategori struct {
	KategoriID uint   `gorm:"primary_key;auto_increment" json:"id"`
	Nama       string `gorm:"not null" json:"nama_kategori"`
}

func (Kategori) TableName() string {
	return "kategori"
}
