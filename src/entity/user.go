package entity

type User struct {
	UserID   uint   `gorm:"primary_key;auto_increment" json:"id"`
	Nama     string `gorm:"not null" json:"nama"`
	Email    string `gorm:"not null" json:"email"`
	Password string `gorm:"not null" json:"password"`

	TipeUserID uint     `json:"tipe_id"`
	TipeUser   TipeUser `gorm:"foreignKey:TipeUserID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL"`
}

func (User) TableName() string {
	return "user"
}
