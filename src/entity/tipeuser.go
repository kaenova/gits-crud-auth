package entity

type TipeUser struct {
	TipeUserID uint   `gorm:"primary_key;auto_increment" json:"id"`
	Nama       string `gorm:"not null" json:"nama_tipe"`
}

func (TipeUser) TableName() string {
	return "tipe_user"
}
