package db

import (
	"os"
	"src/utils/errlogger"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

func initData(db *gorm.DB) {
	/*
		Use this function to make a initial data.
		You need to initialize your data first and the loop through the data.
		To Create Record please refer reading this https://gorm.io/docs/create.html
	*/

	// Kategori
	data, err := os.ReadFile("db/dummy/kategori.sql")
	errlogger.ErrFatalPanic(err)
	db.Exec(string(data))

	// Tipe User
	data, err = os.ReadFile("db/dummy/tipe_user.sql")
	errlogger.ErrFatalPanic(err)
	db.Exec(string(data))

	// User
	data, err = os.ReadFile("db/dummy/user.sql")
	errlogger.ErrFatalPanic(err)
	db.Exec(string(data))

	// Post
	data, err = os.ReadFile("db/dummy/post.sql")
	errlogger.ErrFatalPanic(err)
	db.Exec(string(data))

	// Komentar
	data, err = os.ReadFile("db/dummy/komentar.sql")
	errlogger.ErrFatalPanic(err)
	db.Exec(string(data))

	log.Info().Msg("dummy data terinisialisasi")
}
