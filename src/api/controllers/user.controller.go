package controllers

import (
	"net/http"
	"net/mail"
	"src/api/models"
	"src/entity"
	"src/utils"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

func GetUser(c echo.Context) error {
	var (
		id   string = ""
		nama string = ""

		result models.Response = models.CreateResponse()
		err    error           = nil
	)

	id = c.QueryParam("id")
	nama = c.QueryParam("nama")

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)

	if strings.TrimSpace(id) != "" {
		result.Data, err = models.UserSearchID(id)
		if err != nil {
			return c.JSON(result.Status, result)
		}
		result.Status = http.StatusOK
		result.Message = "Success"
		return c.JSON(result.Status, result)
	}

	if strings.TrimSpace(nama) != "" {
		result.Data, err = models.UserSearchName(nama)
		if err != nil {
			return c.JSON(result.Status, result)
		}
		result.Status = http.StatusOK
		result.Message = "Success"
		return c.JSON(result.Status, result)
	}

	if claims.Id != "" {
		result.Data, err = models.UserSearchID(id)
		if err != nil {
			return c.JSON(result.Status, result)
		}
		result.Status = http.StatusOK
		result.Message = "Success"
		return c.JSON(result.Status, result)
	}

	return c.JSON(result.Status, result)
}

func PostUser(c echo.Context) error {
	// Automatically makes a regular account
	var (
		nama     string
		email    string
		password string

		res models.Response = models.CreateResponse()
	)

	nama = c.FormValue("nama")
	email = c.FormValue("email")
	password = c.FormValue("password")

	if strings.TrimSpace(nama) == "" || strings.TrimSpace(email) == "" || strings.TrimSpace(password) == "" {
		res.Status = http.StatusBadRequest
		res.Message = "Request tidak lengkap."
		return c.JSON(res.Status, res)
	}

	if _, err := mail.ParseAddress(email); err != nil {
		res.Status = http.StatusBadRequest
		res.Message = "Email tidak valid"
		return c.JSON(res.Status, res)
	}

	var err error
	res.Data, err = models.UserCreateUser(nama, email, password)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)

}

func LoginUser(c echo.Context) error {
	var (
		email string
		user  entity.User
		token string

		res models.Response = models.CreateResponse()
		err error           = nil
	)
	email = c.FormValue("email")

	user, err = models.UserSearchEmail(email)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	id := strconv.FormatUint(uint64(user.UserID), 10)
	token, err = utils.GenerateToken(user.Nama, user.Email, id)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}
	res.Data = map[string]string{"token": token, "nama": user.Nama, "email": user.Email}
	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)
}

func UpdateUser(c echo.Context) error {
	// Automatically makes a regular account
	var (
		nama     string
		email    string
		password string

		res models.Response = models.CreateResponse()
	)

	nama = c.FormValue("nama")
	email = c.FormValue("email")
	password = c.FormValue("password")

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)

	user, err := models.UserSearchID(claims.Id)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	if strings.TrimSpace(nama) != "" {
		user.Nama = nama
	}

	if strings.TrimSpace(email) != "" {
		if _, err := mail.ParseAddress(email); err != nil {
			res.Status = http.StatusBadRequest
			res.Message = "Email tidak valid"
			return c.JSON(res.Status, res)
		}
		user.Email = email
	}

	if strings.TrimSpace(password) != "" {
		user.Password = password
	}
	err = models.UserUpdateUser(claims.Id, user)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}
	// ini bawah kayanya cara tersimple cuma ini wkwk
	user.Password = ""

	res.Data = user
	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)

}

func DeleteUser(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)
	id := c.QueryParam("id")

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)

	if strings.TrimSpace(id) != "" {
		err := models.UserDeleteUser(id)
		if err != nil {
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		res.Status = http.StatusOK
		res.Message = "Success"
		return c.JSON(res.Status, res)
	}

	if strings.TrimSpace(claims.Id) != "" {
		err := models.UserDeleteUser(claims.Id)
		if err != nil {
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		res.Status = http.StatusOK
		res.Message = "Success"
		return c.JSON(res.Status, res)
	}
	return c.JSON(res.Status, res)
}
