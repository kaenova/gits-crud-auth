package controllers

import (
	"net/http"
	"src/api/models"
	"strings"

	"github.com/labstack/echo/v4"
)

func GetKomentar(c echo.Context) error {

	var (
		postID string

		res models.Response = models.CreateResponse()
		err error
	)

	postID = c.QueryParam("post_id")

	if strings.TrimSpace(postID) == "" {
		res.Message = "bad request"
		return c.JSON(res.Status, res)
	} else {
		res.Data, err = models.KomentarSearchPostID(postID)
	}

	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)

}
