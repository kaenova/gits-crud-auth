package controllers

import (
	"net/http"
	"src/api/models"
	"strings"

	"github.com/labstack/echo/v4"
)

func GetKategori(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()

		id  string
		err error
	)

	id = c.QueryParam("id")

	if strings.TrimSpace(id) != "" {
		res.Data, err = models.KategoriSearchID(id)
	} else {
		res.Data, err = models.KategoriGetAll()
	}

	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	return c.JSON(res.Status, res)
}
