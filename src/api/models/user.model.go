package models

import (
	"errors"
	"fmt"
	"src/db"
	"src/entity"
)

func UserSearchID(id string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Select("nama", "email").First(&obj, id); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("tidak ditemukan user yang diminta")
	}
	return obj, nil
}

func UserSearchName(name string) (interface{}, error) {
	var (
		mulObj []entity.User
	)
	db := db.GetDB()
	if res := db.Select("nama", "email").Where("nama LIKE ?", fmt.Sprintf("%%%s%%", name)).Find(&mulObj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return nil, res.Error
		}
		return "tidak ditemukan user yang diminta", nil
	}
	return mulObj, nil
}

func UserSearchEmailAuth(email string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Select("nama", "email", "password").Where("email LIKE ?", fmt.Sprintf("%%%s%%", email)).First(&obj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("email tidak ditemukan")
	}
	return obj, nil
}

func UserSearchEmail(email string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Select("user_id", "nama", "email").Where("email LIKE ?", fmt.Sprintf("%%%s%%", email)).First(&obj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("email tidak ditemukan")
	}
	return obj, nil
}

func UserCreateUser(name, email, password string) (interface{}, error) {
	var obj entity.User

	db := db.GetDB()

	result := db.Where("email = ?", email).First(&obj)
	if result.Error == nil || result.RowsAffected != 0 || obj.Email == email {
		return nil, errors.New("email sudah digunakan")
	}

	user := entity.User{Nama: name, Email: email, Password: password, TipeUserID: 1}

	result = db.Select("nama", "email", "password", "tipe_user_id").Create(&user)
	if result.Error != nil || result.RowsAffected == 0 {
		return nil, result.Error
	}

	user.Password = ""

	return user, nil
}

func UserUpdateUser(id string, data entity.User) error {
	var (
		obj entity.User
	)
	db := db.GetDB()

	res := db.Model(&obj).Where("user_id = ?", id).Updates(data)
	if res.RowsAffected == 0 {
		return errors.New("gagal update data")
	}
	return nil
}

func UserDeleteUser(id string) error {
	db := db.GetDB()
	res := db.Delete(&entity.User{}, id)
	if res.RowsAffected == 0 {
		return errors.New("gagal update data")
	}
	return nil
}
