package models

import (
	"errors"
	"src/db"
	"src/entity"
)

func KategoriSearchID(id string) (interface{}, error) {
	var obj entity.Kategori

	db := db.GetDB()

	result := db.Where("kategori_id = ?", id).First(&obj)

	if result.Error != nil {
		return nil, errors.New("server error")
	}

	if result.RowsAffected == 0 {
		return "tidak ditemukan data", nil
	}

	return obj, nil
}

func KategoriGetAll() (interface{}, error) {
	var mulObj []entity.Kategori
	db := db.GetDB()

	result := db.Find(&mulObj)

	if result.Error != nil {
		return nil, errors.New("server error")
	}

	if result.RowsAffected == 0 {
		return "tidak ada data", nil
	}

	return mulObj, nil
}
