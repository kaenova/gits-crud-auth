package models

import (
	"errors"
	"src/db"
	"src/entity"
	"strconv"
)

func KomentarSearchPostID(postID string) (interface{}, error) {
	var obj entity.Komentar

	db := db.GetDB()

	postIDParsed, err := strconv.Atoi(postID)
	if err != nil {
		return nil, err
	}

	result := db.Where("post_id = ?", postIDParsed).First(&obj)

	if result.Error != nil {
		return nil, errors.New("server error")
	}

	if result.RowsAffected == 0 {
		return "tidak ditemukan data", nil
	}

	return obj, nil
}
