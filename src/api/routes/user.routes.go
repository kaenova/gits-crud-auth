package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func User(e *echo.Echo) *echo.Echo {
	// route dibawah ini buat khusus yang butuh jwt/authorisasi
	u := e.Group("/user")
	u.Use(middleware.JWTWithConfig(utils.JWTconfig))
	u.GET("", controllers.GetUser)
	u.PUT("", controllers.UpdateUser)
	u.DELETE("", controllers.DeleteUser)

	// route dibawah ini route publik akses
	e.POST("/user", controllers.PostUser) //Creating Regular Account
	// use custom auth middleware
	e.POST("/login", controllers.LoginUser, utils.Auth)

	return e
}
