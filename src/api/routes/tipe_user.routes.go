package routes

import (
	"src/api/controllers"

	"github.com/labstack/echo/v4"
)

func TipeUser(e *echo.Echo) *echo.Echo {
	e.GET("/tipe", controllers.GetTipeUser)
	// e.PUT("/routes1", ...)
	// e.DELETE("/routes1", ...)
	// e.POST("/tipe", controllers.PostTipeUser) //Creating Regular Account

	return e
}
