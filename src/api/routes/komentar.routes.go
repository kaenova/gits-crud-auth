package routes

import (
	"src/api/controllers"

	"github.com/labstack/echo/v4"
)

func Komentar(e *echo.Echo) *echo.Echo {
	e.GET("/komentar", controllers.GetKomentar)
	// e.PUT("/routes1", ...)
	// e.DELETE("/routes1", ...)
	// e.POST("/tipe", controllers.PostTipeUser) //Creating Regular Account

	return e
}
